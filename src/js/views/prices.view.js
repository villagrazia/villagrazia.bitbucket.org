(function (define) {
    'use strict';

    define([
        'text!../../templates/prices.html'
    ], function (priceTemplate) {

        return {
            url: '/prices',
            template: priceTemplate
        };
    });
}(this.define))
